# ATM Infrastructure Pilot

Collection of Amsterdam Time Machine dataset descriptions in NDE's Requirements for Datasets standard

![Amsterdam Time Machine](https://www.amsterdamtimemachine.nl/wp-content/uploads/2021/07/atm120.png)

## Introduction

In this pilot, we make use of the NDE Dataset Registry (https://datasetregister.netwerkdigitaalerfgoed.nl/) to aggregate relevant datasets for the Amsterdam Time Machine Zuid-Oost pilot. For datasets that are not yet included in the dataset registry, we add them ourselves by providing a metadata file according to the NDE's Requirements for Datasets specifications (https://github.com/netwerk-digitaal-erfgoed/dataset-register-entries). 

## Overview of datasets

### Already included in the registry

### Gathered by us (metadata in json-ld)

## Contributing

## Contact
